﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stressSignTest.Libraries
{
    static class md5Checksum
    {

        private class Rootobject
        {
            public Restservice RESTService { get; set; }
            public List<Response> Response { get; set; }
        }

        private class Restservice
        {
            public string StatusCode { get; set; }
            public string StatusResponse { get; set; }
            public string Message { get; set; }
            public string Fecha { get; set; }
            public string Hora { get; set; }
            public string ResponseKey { get; set; }
            public string ResponseTime { get; set; }
        }

        private class Response
        {
            public int status { get; set; }
            public string statusMessage { get; set; }
            public string name { get; set; }
            public string size { get; set; }
            public string mimeType { get; set; }
            public string md5Checksum { get; set; }
        }

        public static string calculate(string filePath)
        {
            var client = new RestClient(Properties.Settings.Default.wsSignVSha2);
            client.Timeout = -1;
            RestRequest request = new RestRequest(Properties.Settings.Default.md5ChecksumCalculate, Method.POST);
            request.AddHeader("Content-Type", "text/plain");
            request.AddFile("file", filePath);
            IRestResponse<Rootobject> response = client.Execute<Rootobject>(request);

            if (response.ErrorException != null)
                throw response.ErrorException;
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception(response.StatusDescription);
            else if (response.Data == null)
                throw new Exception("No se obtuvo respuesta del servicio");
            else if (response.Data.RESTService.StatusCode != "1")
                throw new Exception(response.Data.RESTService.Message);
            else if (response.Data.Response[0].status == 0)
                throw new Exception(response.Data.Response[0].statusMessage);

            return response.Data.Response[0].md5Checksum;
        }
    }
}
