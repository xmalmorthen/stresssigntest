﻿using Newtonsoft.Json;
using RestSharp;
using stressSignTest.Libraries;
using stressSignTest.models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stressSignTest
{
    class sign
    {
        private RestClient client = null;

        public sign(){
            this.client = new RestClient(Properties.Settings.Default.wsSignVSha2);
            client.Timeout = -1;
        }

        public Rootobject metadata(metadataModel os) {
            string jsonOS = JsonConvert.SerializeObject(os);

            RestRequest request = new RestRequest(Properties.Settings.Default.serviceMetadata,Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", "Basic eG1hbG1vcnRoZW46Li4xMjEyMTJxdw==");
            request.AddParameter("documentId", "4");
            request.AddParameter("originalString", jsonOS);
            request.AddParameter("validator", "stress test");
            IRestResponse<Rootobject> response = client.Execute<Rootobject>(request);

            if (response.ErrorException != null)
                throw response.ErrorException;
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception(response.StatusDescription);
            else if (response.Data == null)
                throw new Exception("No se obtuvo respuesta del servicio");
            else if (response.Data.RESTService.StatusCode != "1")
                throw new Exception(response.Data.RESTService.Message);

            return response.Data;
        }

        public Rootobject file(fileModel os, string filePath, string md5ChecksumResponse = null) {
            FileInfo fi = new FileInfo(filePath);
            
            os.file = new fileInfo()
            {
                name = fi.Name,
                size = fi.Length,
                extension = fi.Extension,
                creationTime = fi.CreationTime.ToShortDateString(),
                accessTime = fi.LastAccessTime.ToShortDateString(),
                updateTime = fi.LastWriteTime.ToShortDateString()
            };

            string jsonOS = JsonConvert.SerializeObject(os);

            if (string.IsNullOrEmpty(md5ChecksumResponse))
                md5ChecksumResponse = md5Checksum.calculate(filePath);

            RestRequest request = new RestRequest(Properties.Settings.Default.serviceFile, Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", "Basic eG1hbG1vcnRoZW46Li4xMjEyMTJxdw==");            
            request.AddParameter("documentId", "4");
            request.AddParameter("originalString", jsonOS);
            request.AddParameter("validator", "stress test");
            request.AddParameter("md5Checksum", md5ChecksumResponse);
            request.AddFile("file", filePath);

            IRestResponse<Rootobject> response = client.Execute<Rootobject>(request);

            if (response.ErrorException != null)
                throw response.ErrorException;
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception(response.StatusDescription);
            else if (response.Data == null)
                throw new Exception("No se obtuvo respuesta del servicio");
            else if (response.Data.RESTService.StatusCode != "1")
                throw new Exception(response.Data.RESTService.Message);

            return response.Data;
        }


    }
}
