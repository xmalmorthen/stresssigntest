﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stressSignTest.models
{

    public class Rootobject
    {
        public Restservice RESTService { get; set; }
        public Response Response { get; set; }
    }

    public class Restservice
    {
        public string StatusCode { get; set; }
        public string StatusResponse { get; set; }
        public string Message { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string ResponseKey { get; set; }
        public string ResponseTime { get; set; }
    }

    public class Response
    {
        public File file { get; set; }
        public Sign sign { get; set; }
        public Evidence evidence { get; set; }
        public Signer signer { get; set; }
        public Token token { get; set; }
    }

    public class Sign
    {
        public string anio { get; set; }
        public string sequence { get; set; }
        public string sign { get; set; }
    }

    public class File
    {
        public string fileName { get; set; }
        public string lenght { get; set; }
        public string mimeType { get; set; }
        public string md5Checksum { get; set; }
    }

    public class Evidence
    {
        public string b64ImgQr { get; set; }
        public string shortUrl { get; set; }
    }

    public class Signer
    {
        public string username { get; set; }
        public string name { get; set; }
        public string charge { get; set; }
        public string email { get; set; }
        public string keyid { get; set; }
        public bool isAdministrator { get; set; }
        public bool isActive { get; set; }
        public Keydata keyData { get; set; }
        public List<Document> documents { get; set; }
    }

    public class Keydata
    {
        public string keyid { get; set; }
        public string serial { get; set; }
        public string description { get; set; }
        public int cryptoengine { get; set; }
        public string organization { get; set; }
        public string organizationUnit { get; set; }
        public string commonName { get; set; }
        public DateTime dateNotBefore { get; set; }
        public DateTime dateNotAfter { get; set; }
        public bool revoked { get; set; }
        public bool pkcs10 { get; set; }
        public Certificateinfo certificateInfo { get; set; }
    }

    public class Certificateinfo
    {
        public string certData { get; set; }
        public object certB64 { get; set; }
        public object publicCertificate { get; set; }
        public object privateKey { get; set; }
        public string getCNTitular { get; set; }
        public string getCNRFC { get; set; }
        public string getSerialNumberTitular { get; set; }
        public string getETitular { get; set; }
        public string getOTitular { get; set; }
        public string getOUTitular { get; set; }
        public string getTTitular { get; set; }
        public string getStreetTitular { get; set; }
        public string getCNEmisor { get; set; }
        public string getSerialNumberEmisor { get; set; }
        public string getEEmisor { get; set; }
        public string getOEmisor { get; set; }
        public string getOUEmisor { get; set; }
        public string getTEmisor { get; set; }
        public string getStreetEmisor { get; set; }
        public bool certExpired { get; set; }
        public string Persona { get; set; }
        public string Emisor { get; set; }
        public string ValidoDesde { get; set; }
        public string ValidoHasta { get; set; }
        public DateTime DvalidoDesde { get; set; }
        public DateTime DvalidoHasta { get; set; }
        public string TamanioClave { get; set; }
        public string NumeroSerie { get; set; }
        public string Hash { get; set; }
        public List<Extensione> Extensiones { get; set; }
    }

    public class Extensione
    {
        public string NombreAmigable { get; set; }
        public object UsoDeClaves { get; set; }
        public object RestriccionesBasicas { get; set; }
        public object IdentificadorDeClaveDeAsunto { get; set; }
        public object UsoMejoradoDeClave { get; set; }
    }

    public class Document
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public bool activo { get; set; }
        public DateTime activeChange { get; set; }
        public Dependencia dependencia { get; set; }
    }

    public class Dependencia
    {
        public int id { get; set; }
        public string nombre { get; set; }
    }

    public class Token
    {
        public DateTime validFrom { get; set; }
        public DateTime validTo { get; set; }
        public string token { get; set; }
        public string originalString { get; set; }
        public string validator { get; set; }
    }

}
