﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stressSignTest.models
{
    class baseModel
    {
            public string description { get; set; }
            public string date { get { return DateTime.Now.ToShortDateString(); } }
    }
}
