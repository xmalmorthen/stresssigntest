﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stressSignTest.models
{
    class fileModel : baseModel { 
        public fileInfo file { get; set; }
    }

    class fileInfo { 
        public string name { get; set; }
        public long size { get; set; }

        public string extension { get; set; }

        public string creationTime { get; set; }
        public string accessTime { get; set; }
        public string updateTime { get; set; }
    }
}
