﻿using stressSignTest.Libraries;
using stressSignTest.models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace stressSignTest
{
    class Program
    {
        //static string basePath = @"E:\Proyectos\visual_studio\cSharp\stresssigntest\";
        static string basePath = @"F:\visual studio\csharp\stressSignTest\stressSignTest\";

        static int metadataProc = 0;
        static int metadataErr = 0;
        static int metadataSucc = 0;
        static int filetProc = 0;
        static int filetErr = 0;
        static int filetSucc = 0;

        private int metadataSignTotal = 0;
        private int fileSignTotal = 0;

        private List<string> filesPath;
        private List<string> md5CheckSum;
        private List<string> metadataSymmaryList = new List<string>();
        private List<string> fileSymmaryList = new List<string>();

        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            int metadataThreadNum = 10;
            int metadataSignsNum = 2500;
            int fileThreadNum = 10;
            int fileSignsNum = 2500;           

            List<Thread> threads = new List<Thread>(metadataThreadNum + fileThreadNum);
            Program baseAction = new Program();

            baseAction.metadataSignTotal = metadataThreadNum * metadataSignsNum;
            baseAction.fileSignTotal = fileThreadNum * fileSignsNum;

            /*
            baseAction.filesPath = new List<string>() {
                 @"E:\basura\50d9a5d4288e4d100863275e112144bd.jpg",
                 @"E:\basura\100wp_5lb_choc_1.jpg",
                 @"E:\basura\face_of_an_eagle_by_zindy-d84jo3s.jpg",
            };
            */

            baseAction.filesPath = new List<string>() {
                 @"D:\Users\XmalMorthen\trash\Caballito_Algoritmo.png",
                 @"D:\Users\XmalMorthen\trash\iconfinder_Cryptocurrency_digital_contract_3287278.png",
                 @"D:\Users\XmalMorthen\trash\logoGobCol.png",
            };

            baseAction.md5CheckSum = new List<string>(baseAction.filesPath.Count());

            foreach (string item in baseAction.filesPath)
                baseAction.md5CheckSum.Add( md5Checksum.calculate(item) );

            for (int i = 0; i < metadataThreadNum; i++)
            {
                ThreadStart mdT = delegate { baseAction.metadataSign(metadataSignsNum); };
                threads.Add(new Thread(mdT));
            }

            for (int i = 0; i < fileThreadNum; i++)
            {
                ThreadStart fleT = delegate { baseAction.fileSign(fileSignsNum); };
                threads.Add(new Thread(fleT));
            }

            stopwatch.Start();
            Console.WriteLine("Iniciando pruebas de estres [ FIRMA DE SERVICIO ]");
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("proceso iniciado en [ {0} - {1} ]",DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
            Console.WriteLine();
            Console.WriteLine("\tIniciando hilos de ejecución");
            Console.WriteLine("\tHilos para firma de metadatos            [ {0} ] \tHilos para firma de archivo             [ {1} ]", metadataThreadNum, fileThreadNum);
            Console.WriteLine("\tCantidad de firma de metadatos por hilo  [ {0} ] \tCantidad de firma de archivo por hilo   [ {1} ]", metadataSignsNum, fileSignsNum);
            Console.WriteLine("\tTotal de firma de metadatos por realizar [ {0} ] \tTotal de firmas de archivo por realizar [ {1} ]", baseAction.metadataSignTotal, baseAction.fileSignTotal);

            Console.ResetColor();
            
            foreach (Thread item in threads)
                item.Start();

            bool isAlive = false;
            do
            {
                isAlive = false;
                foreach (Thread item in threads)
                {
                    if (item.IsAlive)
                    {
                        isAlive = true;
                        break;
                    }
                }
            } while (isAlive);

            try
            {
                baseAction.saveSummary();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine("Ocurrió un error al intentar guardar el sumario de respuesta en archivo \r\n [ {0} ] ", ex.Message);
            }
            stopwatch.Stop();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("proceso terminado en [ {0} - {1} ] tiempo de ejecución [ {2} ] minutos", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(),stopwatch.Elapsed.TotalMinutes);

            Console.ReadKey();
        }

        public void metadataSign(int signCount) {
                        
            for (int i = 0; i <= signCount; i++)
            {
                if (metadataProc >= metadataSignTotal)
                    break;

                string msg = string.Empty;
                try
                {
                    Rootobject response = new sign().metadata(new models.metadataModel()
                    {
                        description = string.Format("metadata sign number [ {0} ]",i)
                    });

                    msg = string.Format("[ succ ] => {0}", response.RESTService.Message); 
                    metadataSucc++;

                }
                catch (Exception ex)
                {
                    msg = string.Format("[ err ] => {0}",ex.Message);
                    metadataErr++;
                }

                metadataProc++;

                Console.SetCursorPosition(8, 10);
                Console.WriteLine("firma de metadatos procesados [ {0} ] \terrores [ {1} ] \texito [ {2} ]", metadataProc, metadataErr, metadataSucc);

                metadataSymmaryList.Add(msg);
            }

            
        }

        public void fileSign(int signCount)
        {
            Random r = new Random(DateTime.Now.Millisecond);

            int idxRandom = r.Next(0, 2);
                        

            for (int i = 0; i <= signCount; i++)
            {
                if (filetProc >= fileSignTotal)
                    break;

                string file = this.filesPath[idxRandom];
                string md5CheckSum = this.md5CheckSum[idxRandom];
                string msg = string.Empty;

                try
                {
                    Rootobject response = new sign().file(
                        new models.fileModel()
                        {
                            description = string.Format("metadata sign number [ {0} ]", i)
                        },
                        file,
                        md5CheckSum
                    );
                    
                    msg = string.Format("[ succ ] => {0}", response.RESTService.Message);
                    filetSucc++;
                }
                catch (Exception ex)
                {
                    filetErr++;
                    msg = string.Format("[ err ] => {0}", ex.Message);
                }

                filetProc++;

                Console.SetCursorPosition(8, 12);
                Console.WriteLine("firma de archivo procesados   [ {0} ] \terrores [ {1} ] \texito [ {2} ]", filetProc, filetErr, filetSucc);
                
                fileSymmaryList.Add(msg);
            }
        }

        public void saveSummary() {
            string metadataFilePath = string.Format("{0}metadataSignSummary_{1}.txt", basePath, DateTime.Now.ToShortDateString().Replace('/','_'));
            string fileFilePath = string.Format("{0}fileSignSummary_{1}.txt", basePath, DateTime.Now.ToShortDateString().Replace('/', '_'));

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Registrando respuesta en archivos");
            Console.WriteLine("Archivo de sumario de firmas de metadatos [ {0} ]", metadataFilePath);
            Console.WriteLine("Archivo de sumario de firmas de archivo [ {0} ]", fileFilePath);

            using (TextWriter tw = new StreamWriter(metadataFilePath)) {
                foreach (string item in metadataSymmaryList)
                    tw.WriteLine("{0}", item);
            }

            using (TextWriter tw = new StreamWriter(fileFilePath))
            {
                foreach (string item in fileSymmaryList)
                    tw.WriteLine("{0}", item);
            }
            
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Registro en archivos generados correctamente...!!!");

        }
    
    }
}
